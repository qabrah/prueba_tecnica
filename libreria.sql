CREATE DATABASE IF NOT EXISTS Libreria;
USE Libreria;

CREATE TABLE Books (
	author VARCHAR(255),
	isbn VARCHAR(255) UNIQUE,
	release_date DATETIME,
	title VARCHAR(500),
	user_id int ,
	users_id int(255),
	id int(255) AUTO_INCREMENT,
	PRIMARY KEY(id)
);
CREATE INDEX users_id ON Books(users_id); 

CREATE TABLE Users (
	email VARCHAR(255) UNIQUE,
	id INT(255) AUTO_INCREMENT,
	name VARCHAR(255),
	password VARCHAR(255),
	PRIMARY KEY(id)
);
