from flask import Flask, request,jsonify
from flask_mysqldb import MySQL
from cryptography.fernet import Fernet



#Conexion MySQL
app = Flask(__name__)
app.config['MYSQL_HOST']='localhost'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']=''
app.config['MYSQL_DB']='libreria'
mysql = MySQL(app)

    
@app.route('/auth/register',methods = ['POST'])
def createuser():
    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        password = request.form['password']
        key = Fernet.generate_key()
        with open("secret.key", "wb") as key_file:
         key_file.write(key)
        key = open("secret.key", "rb").read()
        encoded_message = password.encode()
        f = Fernet(key)
        encrypted_message = f.encrypt(encoded_message)
        cur = mysql.connection.cursor()
        try:
            cur.execute('INSERT INTO users (name,email,password) VALUES (%s,%s,%s)',
            (name,email,encrypted_message))
            mysql.connection.commit()
            cur.close()

            return  'exito'

        except (cur.Error, cur.Warning) as e: 
            print(e)
            return 'eror' 
        
        return 'true'

def clave(valor):
    cifrado = valor
    cifrado2 = cifrado.encode()
    key = open("secret.key", "rb").read()
    f = Fernet(key)
    decryptd_message = f.decrypt(cifrado2)
        
    return decryptd_message

@app.route('/auth/login',methods = ['POST'])
def loginUser():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        cur = mysql.connection.cursor()
        retorno = cur.execute("SELECT email,password FROM users WHERE email=%s",[email]) 
        if retorno > 0:
            rows = cur.fetchall()
            valor = ','.join(rows[0])
            datos = valor.split(",")
            retorno = clave(datos[1])

            
            if (email == datos[0]) and (password.encode() == retorno):
                return 'autorizado'
            else:
                return 'incorrecto'
        else:
            return "Datos no encontrados" 

@app.route('/books',methods = ['GET','POST'])
def showBooks():
    
    if request.method == 'GET':
        cur = mysql.connection.cursor()
        cur.execute('SELECT * FROM books')
        data = cur.fetchall()
        return jsonify(data)
    
    else:
        author = request.form['author']
        isbn = request.form['isbn'] 
        release_date = request.form['release_date']
        title = request.form['title']
        cur = mysql.connection.cursor()
        result = cur.execute('SELECT isbn FROM books WHERE isbn=%s',[isbn])
        if result>0 or author == "" or isbn == "" or release_date == "" or title == "":
            return 'Error'
        cur.execute('INSERT INTO books (author,isbn,release_date,title,user_id,users_id) VALUES (%s,%s,%s,%s,2,2)',
        (author,isbn,release_date,title))
        mysql.connection.commit()
        cur.execute('SELECT id FROM books WHERE isbn=%s',[isbn]) 
        id_libro = cur.fetchall()
        cur.close()
        return jsonify(id_libro)

@app.route('/books/<string:id>',methods = ['DELETE'])
def deletebook(id):
    
    if request.method == 'DELETE':
        cur = mysql.connection.cursor()
        result = cur.execute("DELETE FROM books WHERE id=%s", [id])
        mysql.connection.commit()
        cur.close()
        if result > 0:
            return 'True'
        else:
            return 'Error'
        
        

if __name__ == '__main__': 
    app.run(debug = True)



    


